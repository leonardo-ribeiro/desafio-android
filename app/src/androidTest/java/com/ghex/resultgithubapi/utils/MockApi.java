package com.ghex.resultgithubapi.utils;

import com.ghex.resultgithubapi.providers.ApiManager;
import com.ghex.resultgithubapi.providers.GithubApi;

import net.vidageek.mirror.dsl.Mirror;

import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MockApi {

    public static void setUpServer(MockWebServer server) {

        final ApiManager apiManager = new ApiManager();

        final GithubApi githubApi = new Retrofit.Builder()
                .baseUrl(server.url("/").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubApi.class);

        setField(apiManager, "mGithubApi", githubApi);
    }

    public static void setField(Object target, String fieldName, Object value) {
        new Mirror()
                .on(target)
                .set()
                .field(fieldName)
                .withValue(value);
    }
}
