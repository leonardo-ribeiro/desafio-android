package com.ghex.resultgithubapi;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ghex.resultgithubapi.utils.MockApi;
import com.ghex.resultgithubapi.utils.TestsHelper;
import com.ghex.resultgithubapi.ui.activities.MainActivity;
import com.ghex.resultgithubapi.ui.activities.PullRequestActivity;
import com.ghex.resultgithubapi.utils.Constants;


import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private MockWebServer server;


    @Rule
    public ActivityTestRule<MainActivity> mMainActivityRule = new ActivityTestRule<MainActivity>(MainActivity.class, false, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        MockApi.setUpServer(server);
    }

    @Test
    public void whenResultWithSuccess_shouldDisplayListWithRepositories() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"repositories_success.json")));
        mMainActivityRule.launchActivity(new Intent());
        onView(withId(R.id.repositories_list)).check(matches(isDisplayed()));
    }

    @Test
    public void whenResultWithSuccess_shouldDisplayListItemWithViews() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"single_repositorie_success.json")));
        mMainActivityRule.launchActivity(new Intent());
        onView(withId(R.id.repositories_list)).check(matches(isDisplayed()));
        onView(withId(R.id.owner_avatar)).check(matches(isDisplayed()));
        onView(withId(R.id.owner_name)).check(matches(isDisplayed()));
        onView(withId(R.id.repository_name)).check(matches(isDisplayed()));
        onView(withId(R.id.repository_description)).check(matches(isDisplayed()));
        onView(withId(R.id.repository_forks)).check(matches(isDisplayed()));
        onView(withId(R.id.repository_stars)).check(matches(isDisplayed()));
    }

    @Test
    public void whenResultWithFailure_shouldDisplayErrorMessage() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(400).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"error.json")));
        mMainActivityRule.launchActivity(new Intent());
        onView(withId(R.id.error)).check(matches(isDisplayed()));
    }

    @Test
    public void whenConnectionTimeOut_shouldDisplayErrorMessage() throws Exception {
        server.enqueue(new MockResponse().throttleBody(1024, 1, TimeUnit.SECONDS));
        mMainActivityRule.launchActivity(new Intent());
        onView(withId(R.id.error)).check(matches(isDisplayed()));
    }



    @Test
    public void whenClickOnItemList_shouldStartPullRequestActivity() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"repositories_success.json")));
        mMainActivityRule.launchActivity(new Intent());
        Intents.init();
        Matcher<Intent> matcher = allOf(
                hasComponent(PullRequestActivity.class.getName()),
                hasExtraWithKey(Constants.BUNDLE_INTENT_PULLREQ));


        Instrumentation.ActivityResult
                result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);

        intending(matcher).respondWith(result);

        onView(withId(R.id.repositories_list)).perform(actionOnItemAtPosition(0, click()));

        intended(matcher);
        Intents.release();
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }
}
