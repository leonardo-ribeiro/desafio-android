package com.ghex.resultgithubapi;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ghex.resultgithubapi.utils.MockApi;
import com.ghex.resultgithubapi.utils.TestsHelper;
import com.ghex.resultgithubapi.ui.activities.PullRequestActivity;
import com.ghex.resultgithubapi.utils.Constants;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
public class PullRequestActivityTest {

    MockWebServer server = new MockWebServer();

    @Rule
    public ActivityTestRule<PullRequestActivity> mPullRequestActivityRule = new ActivityTestRule<PullRequestActivity>(PullRequestActivity.class, false, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        MockApi.setUpServer(server);
    }

    @Test
    public void whenResultWithSuccess_shouldDisplayListAndItemWithViews() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"pull_request_success.json")));
        mPullRequestActivityRule.launchActivity(createIntent());
        onView(withId(R.id.pullreq_title)).check(matches(isDisplayed()));
        onView(withId(R.id.pr_author_avatar)).check(matches(isDisplayed()));
        onView(withId(R.id.pr_author_name)).check(matches(isDisplayed()));
        onView(withId(R.id.pullreq_date)).check(matches(isDisplayed()));
        onView(withId(R.id.pullreq_body)).check(matches(isDisplayed()));
    }


    @Test
    public void whenResultWithSuccessAndNoBody_shouldDisplayListAndItemWithViewsAndPredictedMessage() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"single_repositorie_success_no_body.json" )));
        mPullRequestActivityRule.launchActivity(createIntent());
        onView(withId(R.id.pullreq_title)).check(matches(isDisplayed()));
        onView(withId(R.id.pr_author_avatar)).check(matches(isDisplayed()));
        onView(withId(R.id.pr_author_name)).check(matches(isDisplayed()));
        onView(withId(R.id.pullreq_date)).check(matches(isDisplayed()));
        onView(allOf(
                withId(R.id.pullreq_body),
                hasSibling(withText(R.string.no_description)))).check(matches(isDisplayed()));
    }

    @Test
    public void whenResultIsEmpty_shouldDisplayPredictedMessage() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"empty.json")));
        mPullRequestActivityRule.launchActivity(createIntent());
        onView(withId(R.id.no_pulls)).check(matches(isDisplayed()));
    }

    @Test
    public void whenResultWithFailure_shouldDisplayErrorMessage() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(400).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(),"error.json")));
        mPullRequestActivityRule.launchActivity(createIntent());
        onView(withId(R.id.error)).check(matches(isDisplayed()));
    }


    @Test
    public void whenConnectionTimeOut_shouldDisplayErrorMessage() throws Exception {
        server.enqueue(new MockResponse().throttleBody(1024, 1, TimeUnit.SECONDS));
        mPullRequestActivityRule.launchActivity(createIntent());
        onView(withId(R.id.error)).check(matches(isDisplayed()));
    }

    @Test
    public void whenClickOnItemList_shouldStartImplicitIntentWithUrl() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(TestsHelper.getStringFromFile(getInstrumentation().getContext(), "pull_request_success.json")));
        mPullRequestActivityRule.launchActivity(createIntent());
        Intents.init();
        Matcher<Intent> matcher = hasData("https://github.com/ReactiveX/RxJava/pull/5241");

        Instrumentation.ActivityResult
                result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);

        intending(matcher).respondWith(result);

        onView(withId(R.id.pull_request_list)).perform(actionOnItemAtPosition(0, click()));

        intended(matcher);
        Intents.release();
    }


    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }


    private Intent createIntent() {
        Bundle bundleInformacoes = new Bundle();
        bundleInformacoes.putString(Constants.BUNDLE_LOGIN, "login");
        bundleInformacoes.putString(Constants.BUNDLE_NAME, "name");
        return new Intent().putExtra(Constants.BUNDLE_INTENT_PULLREQ, bundleInformacoes);
    }
}
