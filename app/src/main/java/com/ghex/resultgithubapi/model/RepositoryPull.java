package com.ghex.resultgithubapi.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RepositoryPull {

    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    @SerializedName("html_url")
    private String PullReqHtml;
    @SerializedName("created_at")
    private Date date;
    @SerializedName("user")
    private Owner author;

    public RepositoryPull(String title, String body, String pullReqHtml, Date date, Owner author) {
        this.title = title;
        this.body = body;
        PullReqHtml = pullReqHtml;
        this.date = date;
        this.author = author;
    }

    public RepositoryPull() {
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getPullReqHtml() {
        return PullReqHtml;
    }

    public Date getDate() {
        return date;
    }

    public String getFormatedDate() {
        if (date != null) {
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yyyy");
            return fmtOut.format(date);
        } else
            return null;
    }

    public String getAuthorName() {
        return author.getLogin();
    }

    public String getAvatarUrl() {
        return author.getAvatarUrl();
    }
}
