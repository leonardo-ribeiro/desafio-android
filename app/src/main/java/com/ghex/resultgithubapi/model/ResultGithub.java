package com.ghex.resultgithubapi.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResultGithub {

    @SerializedName("items")
    private ArrayList<Item> items;

    public ResultGithub(ArrayList<Item> items) {
        this.items = items;
    }

    public ResultGithub() {
    }

    public ArrayList<Item> getItems() {
        return items;
    }
}
