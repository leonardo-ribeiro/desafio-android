package com.ghex.resultgithubapi.model;

import com.google.gson.annotations.SerializedName;

public class Owner {
    @SerializedName("url")
    private String url;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("login")
    private String login;

    public Owner(String url, String avatarUrl, String login) {
        this.url = url;
        this.avatarUrl = avatarUrl;
        this.login = login;
    }

    public String getUrl() {
        return url;
    }

    public Owner() {
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getLogin() {
        return login;
    }
}
