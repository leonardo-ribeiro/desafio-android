package com.ghex.resultgithubapi.model;

import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("stargazers_count")
    private int stars;
    @SerializedName("forks_count")
    private int forks;
    @SerializedName("owner")
    private Owner owner;

    public Item(String name, String description, int stars, int forks, Owner owner) {
        this.name = name;
        this.description = description;
        this.stars = stars;
        this.forks = forks;
        this.owner = owner;
    }

    public Item() {
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getStars() {
        return stars;
    }

    public int getForks() {
        return forks;
    }

    public String  getOwnerLogin() {
        return owner.getLogin();
    }

    public String getOwnerAvatarUrl() {
        return owner.getAvatarUrl();
    }
}