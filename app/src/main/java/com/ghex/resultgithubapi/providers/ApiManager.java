package com.ghex.resultgithubapi.providers;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public final class ApiManager {

    private static GithubApi mGithubApi;

    public GithubApi getGihubApi() {

        if (mGithubApi == null) {

            mGithubApi = new Retrofit.Builder()
                    .baseUrl(GithubApi.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(GithubApi.class);
        }
        return mGithubApi;
    }
}

