package com.ghex.resultgithubapi.providers;

import com.ghex.resultgithubapi.model.RepositoryPull;
import com.ghex.resultgithubapi.model.ResultGithub;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GithubApi {

    public static final String BASE_URL = "https://api.github.com/";

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<ResultGithub> listRepositories(@Query("page") int page);

    @GET("repos/{login}/{name}/pulls")
    Call<List<RepositoryPull>> listPulls(@Path("login") String login, @Path("name") String name);


}


