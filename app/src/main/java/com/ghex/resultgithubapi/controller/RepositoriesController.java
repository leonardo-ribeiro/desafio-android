package com.ghex.resultgithubapi.controller;

import android.util.Log;

import com.ghex.resultgithubapi.providers.ApiManager;
import com.ghex.resultgithubapi.model.Item;
import com.ghex.resultgithubapi.model.ResultGithub;
import com.ghex.resultgithubapi.ui.activities.MainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class RepositoriesController {

    private static final String TAG = RepositoriesController.class.getSimpleName();
    private MainActivity mListener;
    private ApiManager mApiManager;
    private ResultGithub mResultGithub;

    public RepositoriesController(MainActivity listener) {
        mListener = listener;
        mApiManager = new ApiManager();
    }


    public void startFetching(int page, final boolean refresh) {
        Call<ResultGithub> requestRepositories = mApiManager.getGihubApi().listRepositories(page);
        requestRepositories.enqueue(
                new Callback<ResultGithub>() {
                    @Override
                    public void onResponse(Call<ResultGithub> call, Response<ResultGithub> response) {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "LOG: " + response.code() + " / " + call.request().url());
                            mResultGithub = response.body();
                            mListener.onFetchProgress(mResultGithub.getItems(), refresh);
                        } else {
                            Log.e(TAG, "ERRO: " + response.code());
                            mListener.onFetchFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResultGithub> call, Throwable t) {
                        Log.e(TAG, "ERRO: " + t.getMessage());
                        mListener.onFetchFailed();
                    }
                });
    }

}
