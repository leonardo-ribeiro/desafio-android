package com.ghex.resultgithubapi.controller;

import android.util.Log;

import com.ghex.resultgithubapi.providers.ApiManager;
import com.ghex.resultgithubapi.model.RepositoryPull;
import com.ghex.resultgithubapi.ui.activities.PullRequestActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullReqItemsController{

    private static final String TAG = PullReqItemsController.class.getSimpleName();
    private ApiManager mApiManager;
    private List<RepositoryPull> mRepositoryPulls;
    private PullRequestActivity mListener;
    private String mName;
    private String mLogin;

    public PullReqItemsController(String login, String name, PullRequestActivity listener) {
        mName = name;
        mLogin = login;
        mListener = listener;
        mApiManager = new ApiManager();
    }

    public void startFetching() {
        Call<List<RepositoryPull>> requestPullReqList = mApiManager.getGihubApi().listPulls(mLogin,mName);
        requestPullReqList.enqueue(new Callback<List<RepositoryPull>>() {
            @Override
            public void onResponse(Call<List<RepositoryPull>> call, Response<List<RepositoryPull>> response) {
                if (response.isSuccessful()) {
                    mRepositoryPulls = response.body();
                    mListener.onFetchProgress(mRepositoryPulls, false);
                } else {
                    Log.e(TAG, "ERRO: " + response.code() + call.request().url());
                    mListener.onFetchFailed();
                }
            }

            @Override
            public void onFailure(Call<List<RepositoryPull>> call, Throwable t) {
                Log.e(TAG, "ERRO: " + t.getMessage());
                mListener.onFetchFailed();
            }
        });
    }

}
