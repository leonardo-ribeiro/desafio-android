package com.ghex.resultgithubapi.controller;

import java.util.List;

public interface CallBackListener {

    void onFetchProgress (List<?> list, boolean refresh);

    void onFetchComplete();

    void onFetchFailed();

}
