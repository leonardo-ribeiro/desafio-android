package com.ghex.resultgithubapi.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghex.resultgithubapi.R;
import com.ghex.resultgithubapi.utils.CircleTransform;
import com.ghex.resultgithubapi.model.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepositoriesItemAdapter extends RecyclerView.Adapter<RepositoriesItemAdapter.ItemViewHolder> {

    private List<Item> mItems;

    final private RepositoriesOnClickHandler mClickHandler;

    public interface RepositoriesOnClickHandler {
        void onClick(String login, String name);
    }


    public RepositoriesItemAdapter(List<Item> items, RepositoriesOnClickHandler clickHandler) {
        this.mItems = items;
        mClickHandler = clickHandler;
    }

    public void addItem(Item item) {
        mItems.add(item);
        notifyDataSetChanged();
    }

    public void addItemList(List<Item> items, boolean refresh) {
        if(refresh){
            mItems.clear();
            mItems.addAll(items);
        }else{
            mItems.addAll(items);
        }
        notifyDataSetChanged();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_repositories_item, viewGroup, false);
        return  new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder viewHolder, int position) {
        Item item = mItems.get(position);

        if (item != null) {
            viewHolder.mRepositoryName.setText(item.getName());
            viewHolder.mRepositorydescription.setText(item.getDescription());
            viewHolder.mRepositoryForks.setText(String.valueOf(item.getForks()));
            viewHolder.mRepositoryStars.setText(String.valueOf(item.getStars()));
            viewHolder.mOwnerName.setText(item.getOwnerLogin());

            Picasso.with(viewHolder.itemView.getContext())
                    .load(item.getOwnerAvatarUrl())
                    .transform(new CircleTransform())
                    .into(viewHolder.mOwnerAvatar);

        }
    }
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.repository_name)
        TextView mRepositoryName;
        @BindView(R.id.repository_description)
        TextView mRepositorydescription;
        @BindView(R.id.repository_forks)
        TextView mRepositoryForks;
        @BindView(R.id.repository_stars)
        TextView mRepositoryStars;
        @BindView(R.id.owner_name)
        TextView mOwnerName;
        @BindView(R.id.owner_avatar)
        ImageView mOwnerAvatar;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            mClickHandler.onClick(mItems.get(adapterPosition).getOwnerLogin(),mItems.get(adapterPosition).getName());
        }
    }

}
