package com.ghex.resultgithubapi.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghex.resultgithubapi.R;
import com.ghex.resultgithubapi.model.RepositoryPull;
import com.ghex.resultgithubapi.utils.RoundedCornersTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PullReqItemAdapter extends RecyclerView.Adapter<PullReqItemAdapter.ItemViewHolder> {

    private List<RepositoryPull> mRepositoryPulls;
    private PullReqAdapterOnClickHandler mClickHandler;

    public interface PullReqAdapterOnClickHandler {
        void onClick(String url);
    }

    public PullReqItemAdapter(List<RepositoryPull> RepositoryPulls, PullReqAdapterOnClickHandler clickHandler) {
        mRepositoryPulls = RepositoryPulls;
        mClickHandler = clickHandler;
    }

    public void addPullReqList(List<RepositoryPull> repositoryPulls) {
        mRepositoryPulls.clear();
        mRepositoryPulls.addAll(repositoryPulls);
        notifyDataSetChanged();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_pullreq_item, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder viewHolder, int position) {
        RepositoryPull repository = mRepositoryPulls.get(position);

        if (repository != null) {
            viewHolder.mPullReqTitle.setText(repository.getTitle());
            viewHolder.mPullReqDate.setText(repository.getFormatedDate());
            viewHolder.mAuthorName.setText(repository.getAuthorName());

            Picasso.with(viewHolder.itemView.getContext())
                    .load(repository.getAvatarUrl())
                    .transform(new RoundedCornersTransform())
                    .into(viewHolder.mAuthorAvatar);

            if (!mRepositoryPulls.get(position).getBody().isEmpty())
                viewHolder.mPullReqBody.setText(repository.getBody());
            else
                viewHolder.mPullReqBody.setText(R.string.no_description);
        }
    }

    @Override
    public int getItemCount() {
        return mRepositoryPulls.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.pullreq_title)
        TextView mPullReqTitle;
        @BindView(R.id.pullreq_body)
        TextView mPullReqBody;
        @BindView(R.id.pullreq_date)
        TextView mPullReqDate;
        @BindView(R.id.pr_author_name)
        TextView mAuthorName;
        @BindView(R.id.pr_author_avatar)
        ImageView mAuthorAvatar;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            mClickHandler.onClick(mRepositoryPulls.get(adapterPosition).getPullReqHtml());
        }
    }
}
