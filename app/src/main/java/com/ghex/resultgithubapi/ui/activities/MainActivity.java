package com.ghex.resultgithubapi.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ghex.resultgithubapi.R;
import com.ghex.resultgithubapi.controller.CallBackListener;
import com.ghex.resultgithubapi.controller.RepositoriesController;
import com.ghex.resultgithubapi.ui.adapters.RepositoriesItemAdapter;
import com.ghex.resultgithubapi.model.Item;
import com.ghex.resultgithubapi.utils.Constants;
import com.ghex.resultgithubapi.utils.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        RepositoriesItemAdapter.RepositoriesOnClickHandler,
        CallBackListener {

    private RepositoriesController mController;
    private RepositoriesItemAdapter mAdapter;
    private List<Item> mItems = new ArrayList<>();
    private int mPage = 1;
    private LinearLayoutManager mLinearLayout;

    @BindView(R.id.repositories_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.error)
    LinearLayout mNoInternet;
    @BindView(R.id.text_error)
    TextView mTextError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mController = new RepositoriesController(MainActivity.this);
        configToolbar();
        configViews();
        progressBarOn();
        mController.startFetching(mPage, false);
    }

    private void configToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void configViews() {

        mLinearLayout = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setLayoutManager(mLinearLayout);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new RepositoriesItemAdapter(mItems, this);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLinearLayout) {
            @Override
            public void onLoadMore(int currentPage) {
                mController.startFetching(currentPage, false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.colorAccent),
                ContextCompat.getColor(this, R.color.colorPrimary),
                ContextCompat.getColor(this, R.color.colorPrimaryDark));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mNoInternet.getVisibility() == View.VISIBLE) {
                    refreshFromError();
                }
                mPage = 1;
                mController.startFetching(mPage, true);

            }
        });
    }

    @Override
    public void onFetchProgress(List<?> items, boolean refresh) {
        mAdapter.addItemList((List<Item>) items, refresh);
        onFetchComplete();
    }

    @Override
    public void onFetchComplete() {
        progressBarOff();
        swipeRefreshOff();
        mRecyclerView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onFetchFailed() {
        progressBarOff();
        swipeRefreshOff();

        if (mLinearLayout.getItemCount() > 2) {
            Toast.makeText(this, R.string.erro_Carregamento, Toast.LENGTH_LONG).show();
        } else {

            mRecyclerView.setVisibility(View.GONE);
            if (!isOnline()) {
                mNoInternet.setVisibility(View.VISIBLE);
            } else {
                mNoInternet.setVisibility(View.VISIBLE);
                mTextError.setText(R.string.error);
            }
        }
    }

    @Override
    public void onClick(String login, String name) {
        Bundle bundleInformacoes = new Bundle();
        bundleInformacoes.putString(Constants.BUNDLE_LOGIN, login);
        bundleInformacoes.putString(Constants.BUNDLE_NAME, name);
        Intent intentMudancaTela = new Intent(this, PullRequestActivity.class);
        intentMudancaTela.putExtra(Constants.BUNDLE_INTENT_PULLREQ, bundleInformacoes);
        startActivity(intentMudancaTela);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void progressBarOn() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void progressBarOff() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private void swipeRefreshOff() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void refreshFromError() {
        mNoInternet.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }
}
