package com.ghex.resultgithubapi.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ghex.resultgithubapi.R;
import com.ghex.resultgithubapi.controller.CallBackListener;
import com.ghex.resultgithubapi.controller.PullReqItemsController;
import com.ghex.resultgithubapi.ui.adapters.PullReqItemAdapter;
import com.ghex.resultgithubapi.model.RepositoryPull;
import com.ghex.resultgithubapi.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestActivity extends AppCompatActivity implements
        CallBackListener,
        PullReqItemAdapter.PullReqAdapterOnClickHandler {

    private PullReqItemAdapter mPullReqItemAdapter;
    private PullReqItemsController mPullReqItemsController;
    private List<RepositoryPull> mRepositoryPulls = new ArrayList<>();
    private String mName;
    private String mLogin;

    @BindView(R.id.no_pulls) LinearLayout mNoPulls;
    @BindView(R.id.error) LinearLayout mNoInternet;
    @BindView(R.id.text_error) TextView mTextError;
    @BindView(R.id.pull_request_list) RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.toolbar) Toolbar mToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        ButterKnife.bind(this);

        Intent intentPassada = getIntent();
        Bundle bundle = intentPassada.getBundleExtra(Constants.BUNDLE_INTENT_PULLREQ);
        mName = bundle.getString(Constants.BUNDLE_NAME);
        mLogin = bundle.getString(Constants.BUNDLE_LOGIN);

        configToolbar();
        configViews();
        mPullReqItemsController = new PullReqItemsController(mLogin, mName, this);
        mPullReqItemsController.startFetching();
    }

    private void configToolbar() {
        mToolbar.setTitle(mName);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void configViews() {
        progressBarOn();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(PullRequestActivity.this));
        mRecyclerView.setHasFixedSize(true);
        mPullReqItemAdapter = new PullReqItemAdapter(mRepositoryPulls, this);
        mRecyclerView.setAdapter(mPullReqItemAdapter);
    }

    @Override
    public void onFetchProgress(List<?> repositoryPulls, boolean refresh) {
        if (repositoryPulls.isEmpty()) {
            noPulls();
        } else {
            mPullReqItemAdapter.addPullReqList((List<RepositoryPull>) repositoryPulls);
            onFetchComplete();
        }
    }

    @Override
    public void onFetchComplete() {
        progressBarOff();
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFetchFailed() {
        progressBarOff();

        mRecyclerView.setVisibility(View.GONE);

        if (!isOnline()) {
            mNoInternet.setVisibility(View.VISIBLE);
        } else {
            mNoInternet.setVisibility(View.VISIBLE);
            mTextError.setText(R.string.error);
        }
    }

    @Override
    public void onClick(String url) {
        Intent callBrowser = new Intent(Intent.ACTION_VIEW);
        if (url != null) {
            callBrowser.setData(Uri.parse(url));
            startActivity(callBrowser);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void noPulls() {
        progressBarOff();
        mRecyclerView.setVisibility(View.GONE);
        mNoPulls.setVisibility(View.VISIBLE);
    }

    public void progressBarOn() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void progressBarOff() {
        mProgressBar.setVisibility(View.GONE);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
